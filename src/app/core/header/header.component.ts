import { Component, HostListener, OnInit } from '@angular/core';
import {AuthService} from "../authentification/auth.service";
import {faUser} from "@fortawesome/free-regular-svg-icons";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {
  isScroll = false;
  menuOpen = false;
  loggedUser: any;
  dropdown = false;
  faUser = faUser;

  @HostListener('window:scroll', [])
  onWindowScroll() {
    const scrollPosition = window.scrollY || document.documentElement.scrollTop || document.body.scrollTop || 0;
    this.isScroll = scrollPosition >= 20;
  }

  @HostListener('document:click', ['$event'])
  clickout(event: any) {
    if (!event.target.classList.contains('profil-wrapper') && !event.target.closest('.mon-compte')) {
      this.dropdown = false;
    }
  }


  constructor(private authService: AuthService) {
  }

  ngOnInit(): void {
    this.authService.getUserProfile().subscribe((res: any) => {
      this.loggedUser = res;
    })
  }

  logout(): void {
    this.authService.doLogout();
  }

  toggleMenu() {
    this.menuOpen = !this.menuOpen;
  }

  toggleDropdown() {
    this.dropdown = !this.dropdown;
  }
}
