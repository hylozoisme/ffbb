import { Injectable } from "@angular/core";
import { HttpInterceptor, HttpRequest, HttpHandler } from "@angular/common/http";
import { AuthService } from "../authentification/auth.service";
import { Router } from "@angular/router";
import { switchMap, catchError } from "rxjs/operators";
import { throwError } from "rxjs";

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(private authService: AuthService, private router: Router) { }

  intercept(req: HttpRequest<any>, next: HttpHandler) {
    const authToken = this.authService.getToken();
    const refresh_token = this.authService.getRefreshToken();

    if (req.url.includes('/login') || req.url.includes('/register') || req.url.includes('/token/refresh')) {
      return next.handle(req);
    }

    if (!authToken) {
      if (!refresh_token) {
        this.router.navigate(['/connexion']);
        return throwError(() => new Error("No refresh token found"));
      }

      return this.authService.refreshToken().pipe(
        switchMap((res: any) => {
          if (!res.token || !res.refresh_token) {
            this.router.navigate(['/connexion']);
            return throwError(() => new Error("No new tokens received"));
          }

          localStorage.setItem('access_token', res.token);
          localStorage.setItem('refresh_token', res.refresh_token);

          req = req.clone({
            setHeaders: {
              Authorization: "Bearer " + res.token,
              Accept: "application/json",
            }
          });

          return next.handle(req);
        }),
        catchError(error => {
          this.router.navigateByUrl('/connexion');
          return throwError(error);
        })
      );
    }


    req = req.clone({
      setHeaders: {
        Authorization: "Bearer " + authToken,
        Accept: "application/json",
      }
    });

    return next.handle(req).pipe(
      catchError(error => {
        if (error.status === 401 && refresh_token) {
          return this.authService.refreshToken().pipe(
            switchMap((res: any) => {
              localStorage.setItem('access_token', res.token);
              localStorage.setItem('refresh_token', res.refresh_token);

              req = req.clone({
                setHeaders: {
                  Authorization: "Bearer " + res.token,
                  Accept: "application/json",
                }
              });

              return next.handle(req);
            }),
            catchError(error => {
              if (error.status === 401) {
                this.router.navigateByUrl('/connexion');
              }

              return throwError(error);
            })
          );
        } else {
          return throwError(error);
        }
      })
    );
  }
}
