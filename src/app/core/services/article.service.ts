import { Injectable } from '@angular/core';
import {Observable, Subject} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class ArticleService {
  private articleClicked = new Subject<number>();
  articleClicked$ = this.articleClicked.asObservable();

  private modalOpen = false;
  private modalOpenSubject = new Subject<boolean>();
  modalOpen$ = this.modalOpenSubject.asObservable();

  constructor(private http: HttpClient) { }

  openModal() {
    this.modalOpen = true;
    this.modalOpenSubject.next(true);
  }

  closeModal() {
    this.modalOpen = false;
    this.modalOpenSubject.next(false);
  }

  isModalOpen(): Observable<boolean> {
    return this.modalOpenSubject.asObservable();
  }

  clickArticle(article: any) {
    this.articleClicked.next(article);
  }

  getArticle(id: string){
    return this.http.get(environment.api + '/articles/' + id);
  }

  userLearnedArticle(article: any) {
    return this.http.post(environment.api + '/articles/' + article.id + '/user_learned', {});
  }
}
