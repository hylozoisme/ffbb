import { TestBed } from '@angular/core/testing';

import { ArticleNavigationService } from './article-navigation.service';

describe('ArticleNavigationService', () => {
  let service: ArticleNavigationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ArticleNavigationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
