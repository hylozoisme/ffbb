import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import { environment } from "../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class ReglesService {

  constructor(private http: HttpClient) { }

  getRegles() {
    return this.http.get(environment.api + '/regles');
  }
}
