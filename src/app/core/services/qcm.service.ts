import {EventEmitter, Injectable} from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { BehaviorSubject } from "rxjs";
import { Router } from "@angular/router";
import { environment } from "../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class QcmService {

  private questions: any[] = [];
  private currentQuestionIndex = new BehaviorSubject<number>(0);
  private uuid =  '';
  private answerResponse: any;
  private _showFeedback = new BehaviorSubject(false);
  submittedAnswers: any[] = [];
  _userAnswerStatus = new EventEmitter<boolean>();

  constructor(private http: HttpClient, private router: Router) { }

  generateQcm() {
    this.questions = [];
    this.currentQuestionIndex.next(0);

    return this.http.post(environment.api + '/qcm/generate', { });
  }

  getQcm(uuid: string) {
    this.answerResponse = undefined;
    this._showFeedback.next(false);
    this.submittedAnswers = [];


    return this.http.get(environment.api + '/qcm/' + uuid).subscribe(
      (data: any) => {
        this.questions = data.questions;
        this.uuid = uuid;
        this.currentQuestionIndex.next(data?.current_question);
      },
      (error) => {
        console.log(error);
        this.router.navigate(['/']);
      }
    );
  }

  getQcmByUuid(uuid: string) {
    return this.http.get(environment.api + '/qcm/' + uuid);
  }

  getCurrentQuestion() {
    return this.questions[this.currentQuestionIndex.value];
  }

  getCurrentQuestionIndex() {
    return this.currentQuestionIndex.asObservable();
  }

  goToNextQuestion(): boolean {
    if (this.currentQuestionIndex.value < this.questions.length - 1) {
      this.currentQuestionIndex.next(this.currentQuestionIndex.value + 1);
      this.answerResponse = [];
      this.hideFeedback();
      this._userAnswerStatus.emit(undefined);
    }

    return this.isLastQuestion();
  }

  isLastQuestion(): boolean {
    return this.currentQuestionIndex.value === this.questions.length - 1;
  }


  getAnswerResponse() {
    return this.answerResponse;
  }

  getShowFeedback() {
    return this._showFeedback.asObservable();
  }

  hideFeedback() {
    this._showFeedback.next(false);
  }

  showFeedback(){
    this._showFeedback.next(true);
  }

  submitAnswers(answers: any) {
    this.submittedAnswers = answers;

    this.http.post(environment.api + '/qcm/answer', {
      answers: answers,
      qcm_id: this.uuid,
      question_id: this.getCurrentQuestion().question.id
    }).subscribe(
      (data: any) => {
        this.answerResponse = data;
        this.showFeedback();

        const isCorrect = this.isUserAnswerCorrect();
        this._userAnswerStatus.emit(isCorrect);
      }, (error) => {
        console.log(error);
      }
    );
  }

  getCorrectAnswerIds(): string[] {
    const correctAnswers = this.answerResponse?.question?.questionPropositions;
    if (!correctAnswers) {
      return [];
    }

    return correctAnswers
      .filter((prop: any) => prop.is_answer)
      .map((prop: any) => prop.value);
  }


  isUserAnswerCorrect() {
    const correctAnswers = this.answerResponse?.question?.questionPropositions;
    if (!correctAnswers) {
      return false;
    }

    const correctAnswerIds = correctAnswers
      .filter((prop: any) => prop.is_answer)
      .map((prop: any) => prop.value);

    return (
      this.submittedAnswers.length === correctAnswerIds.length &&
      this.submittedAnswers.every((answer) => correctAnswerIds.includes(answer))
    );
  }

  getQcms(page = 1) {
    return this.http.get(environment.api + '/qcms?page=' + page);
  }

  getAllQcms() {
    return this.http.get(environment.api + '/qcms/all');
  }

  viewResults() {
    this.router.navigate(['/entrainement/' + this.uuid + '/result']);
  }
}
