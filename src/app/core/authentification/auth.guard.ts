import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';
@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(
    public authService: AuthService,
    public router: Router
  ) { }
  canActivate(): Observable<boolean> | Promise<boolean> | boolean {
    const token = this.authService.getToken();
    if (!token) {
      this.router.navigate(['connexion']);
      return false;
    }

    const isValid = this.authService.validateToken(token);
    if (!isValid) {
      this.router.navigate(['connexion']);
      return false;
    }

    return true;
  }
}
