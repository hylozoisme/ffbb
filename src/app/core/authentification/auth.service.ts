import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { map } from 'rxjs/operators';
import {
  HttpClient,
  HttpHeaders,
  HttpErrorResponse,
} from '@angular/common/http';
import { Router } from '@angular/router';
import jwt_decode from "jwt-decode";
import { environment } from "../../../environments/environment";

interface JwtPayload {
  exp: number;
}


@Injectable({
  providedIn: 'root',
})

export class AuthService {

  public loginError = false;
  public loading = false;
  endpoint: string = environment.api;
  headers = new HttpHeaders().set('Content-Type', 'application/json');

  constructor(private http: HttpClient, public router: Router) { }

  // Sign-up
  register(username: string, password: string, nom: string, prenom: string): Observable<any> {
    const api = `${this.endpoint}/register`;

    return this.http.post(api, { "username": username, "password": password, "nom": nom, "prenom": prenom })
  }

  // Sign-in
  signIn(username: string, password: string) {
    this.loading = true;

    return this.http
      .post<any>(`${this.endpoint}/login`, {"username": username, "password": password})
      .subscribe((res: any) => {
        this.loading = false;
        localStorage.setItem('access_token', res.token);
        localStorage.setItem('refresh_token', res.refresh_token);
        this.router.navigate(['/']);
      },
        (err: HttpErrorResponse) => {
          this.loading = false;
          this.loginError = true;
        }
      );
  }

  validateToken(token: string): boolean {
    try {
      const decoded: JwtPayload = jwt_decode(token);
      const currentTime = Date.now() / 1000;
      if (decoded && decoded.exp && decoded.exp > currentTime) {
        return true;
      }
    } catch (error) {
      console.error(error);
    }
    return false;
  }

  refreshToken(){
    return this.http
      .post<any>(`${this.endpoint}/token/refresh`, {"refresh_token": this.getRefreshToken()})
  }

  getToken() {
    return localStorage.getItem('access_token');
  }

  getRefreshToken(){
    return localStorage.getItem('refresh_token');
  }

  get isLoggedIn(): boolean {
    const authToken = localStorage.getItem('access_token');
    return authToken !== null;
  }

  doLogout() {
    const removeToken = localStorage.removeItem('access_token');
    const removeRefreshToken = localStorage.removeItem('refresh_token');

    if (removeToken == null && removeRefreshToken == null) {
      this.router.navigate(['connexion']);
    }
  }

  getUserProfile() {
    const api = `${this.endpoint}/user/profil`;
    return this.http.get(api, { headers: this.headers }).pipe(
      map((res: any) => {
        return res || {}
      })
    );
  }

  handleError(error: HttpErrorResponse) {
    let msg = '';
    if (error.error instanceof ErrorEvent) {
      msg = error.error.message;
    } else {
      msg = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    return throwError(msg);
  }
}
