export interface UserProfil {
  nom: string;
  prenom: string;
  photo: string;
  niveau: string;
}
