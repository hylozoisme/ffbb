import { Article} from "./article";

export interface Regle {
  id: number;
  libelle: string;
  photo: string;
  slug: string;
  articles: Article[];
}
