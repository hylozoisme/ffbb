export interface Question {
  id: string;
  question: string;
  reponse: string[];
  proposition: string[];
  feedback: string;
  article: string;
}
