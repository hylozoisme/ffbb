import {Regle} from "./regle";

export interface Article {
  id: number;
  libelle: string;
  description: string;
  photo: string;
  regle: Regle;
  children: Article[];
  slug: string;
  time: number;
  position: number;
  is_learned: boolean;
  is_main_article: boolean;
}
