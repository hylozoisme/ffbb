import {Component, Input, OnInit, AfterViewInit, Renderer2} from '@angular/core';
import {faArrowRight, faCheck} from "@fortawesome/free-solid-svg-icons";
import {Router} from "@angular/router";
import {ArticleService} from "../../../core/services/article.service";

@Component({
  selector: 'app-article-navigation',
  templateUrl: './article-navigation.component.html',
  styleUrls: ['./article-navigation.component.scss']
})
export class ArticleNavigationComponent {
  @Input() article: any;
  @Input() articleParent: any;
  faArrowRight = faArrowRight;
  faCheck = faCheck;

  constructor(private articleService: ArticleService) { }

  nextArticle() {
    const nextPosition = this.article.position + 1;
    if (nextPosition > this.articleParent.childrens.length) {
      return;
    }

    this.articleService.userLearnedArticle(this.article);

    this.article = this.articleParent.childrens[this.article.position];
  }
}
