import {Component, Input} from '@angular/core';
@Component({
  selector: 'app-regles',
  templateUrl: './regles.component.html',
  styleUrls: ['./regles.component.scss']
})
export class ReglesComponent {
  @Input() regle: any;
}
