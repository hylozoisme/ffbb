import {Component, Input} from '@angular/core';
import {ArticleService} from "../../../../core/services/article.service";

@Component({
  selector: 'app-regles-article',
  templateUrl: './regles-article.component.html',
  styleUrls: ['./regles-article.component.scss']
})
export class ReglesArticleComponent {

  @Input() article: any;
  widthOfBar = `width: ${0} %`;

  constructor(private articleService: ArticleService) { }

  // Allow to update the width of the bar when the article is loaded
  ngOnChanges() {
    if(this.article && this.article.nb_of_child_learned != 0 && this.article.childrens.length != 0) {
      const nbOfChildLearned = Number(this.article.nb_of_child_learned);
      const nbOfChild = Number(this.article.childrens.length);

      this.widthOfBar = `width: ${nbOfChildLearned / nbOfChild * 100}%`;
    }
  }

  // Openup the modal with the article and it's childrens
  onArticleClick(article: any) {
    this.articleService.clickArticle(article);
  }
}
