import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReglesArticleComponent } from './regles-article.component';

describe('ReglesArticleComponent', () => {
  let component: ReglesArticleComponent;
  let fixture: ComponentFixture<ReglesArticleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReglesArticleComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ReglesArticleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
