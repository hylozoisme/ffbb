import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { faTimes } from '@fortawesome/free-solid-svg-icons';
import { NotificationService } from "../../../core/services/notification.service";

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss'],
  animations: [
    trigger('slideInRight', [
      state('void', style({transform: 'translateX(40%)', opacity: 0})),
      state('*', style({transform: 'translateX(0)', opacity: 1})),
      transition('void => *', animate('250ms ease-in')),
      transition('* => void', animate('100ms ease-out'))
    ])
  ]

})
export class NotificationComponent implements OnInit, OnDestroy {
  message: string | null | undefined;
  private subscription!: Subscription;
  faTimes = faTimes;

  constructor(private notificationService: NotificationService) { }

  ngOnInit() {
    this.subscription = this.notificationService.notification$.subscribe(msg => {
      this.message = msg;
      setTimeout(() => {
        this.message = null;
      }, 3000);
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
