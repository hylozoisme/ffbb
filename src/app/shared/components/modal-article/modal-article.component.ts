import {Component, Input} from '@angular/core';
import {faCheck, faTimes} from '@fortawesome/free-solid-svg-icons';
import { faBook } from '@fortawesome/free-solid-svg-icons';
import {ArticleService} from "../../../core/services/article.service";

@Component({
  selector: 'app-modal-article',
  templateUrl: './modal-article.component.html',
  styleUrls: ['./modal-article.component.scss']
})
export class ModalArticleComponent {
  faCheck = faCheck;
  faTimes = faTimes;
  faBookMark = faBook;
  article: any;
  modalOpen = false;

  constructor(private articleService: ArticleService) {
  }

  ngOnInit() {
    this.articleService.articleClicked$.subscribe((article: any) => {
      this.articleService.openModal();
      this.article = article;
    });

    this.articleService.isModalOpen().subscribe((modalOpen: boolean) => {
      this.modalOpen = modalOpen;
    });
  }


  closeModal() {
    this.articleService.closeModal();
  }
}
