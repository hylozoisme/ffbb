import {Component, Input, OnChanges} from '@angular/core';
import {ChartConfiguration, ChartType} from "chart.js";

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.scss']
})
export class ChartComponent {
  @Input() qcms: any;
  data: any[] = [];
  chartLabels: any[] = [];

  lineChartLabels: any[] = [];
  lineChartLegend = false;
  lineChartPlugins = [];
  lineChartData: any[] = [];

  public lineChartOptions: ChartConfiguration['options'] = {
    responsive: true,

    elements: {
      line: {
        tension: 0,
      }
    },
    scales: {
      y: {
        min: 0,
        max: 20,
        ticks: {
          stepSize: 5,
        },
        position: 'left',
        },

      x: {
        grid: {
          display: false,
        }
      }
    },

    plugins: {
      legend: { display: false },
    },
  };

  public lineChartType: ChartType = 'line';

  ngOnChanges(): void {
    this.qcms.forEach((qcm: any) => {

      parseInt(qcm.note)


      const date = new Date(qcm.date);
      const month = date.toLocaleString('fr-FR', { month: '2-digit' });
      const date_qcm = date.getDate() + '/' + month;

      this.data.push(qcm.note);
      this.chartLabels.push(date_qcm);
    })

    this.lineChartData = [
      {data: this.data, label: 'Note', pointRadius: 4, borderColor: 'rgba(8,56,139,0.4)',
        pointColor: '#08388BFF', pointBackgroundColor: '#08388BFF'},
    ];

    this.lineChartLabels = this.chartLabels;
  }
}
