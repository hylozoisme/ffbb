import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QuestionCorrectionComponent } from './question-correction.component';

describe('QuestionCorrectionComponent', () => {
  let component: QuestionCorrectionComponent;
  let fixture: ComponentFixture<QuestionCorrectionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QuestionCorrectionComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(QuestionCorrectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
