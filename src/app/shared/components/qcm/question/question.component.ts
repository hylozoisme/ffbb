import {Component, EventEmitter, Input, Output} from '@angular/core';
import {QcmService} from "../../../../core/services/qcm.service";
import {DomSanitizer, SafeResourceUrl} from '@angular/platform-browser';



@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.scss']
})
export class QuestionComponent {

  @Input() question: any;
  @Input() questionIndex = 0;
  @Output() answered = new EventEmitter();
  url: SafeResourceUrl = ''

  constructor(public qcmService: QcmService, private sanitizer: DomSanitizer) { }

  submitAnswerAndGoToNextQuestion(answers: any) {
    this.qcmService.submitAnswers(answers)
  }

  ngOnChanges() {
    this.url = this.sanitizer.bypassSecurityTrustResourceUrl(this.question?.question?.questionFile?.file_url);
  }
}
