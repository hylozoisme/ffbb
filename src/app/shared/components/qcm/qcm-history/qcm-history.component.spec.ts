import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QcmHistoryComponent } from './qcm-history.component';

describe('QcmHistoryComponent', () => {
  let component: QcmHistoryComponent;
  let fixture: ComponentFixture<QcmHistoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QcmHistoryComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(QcmHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
