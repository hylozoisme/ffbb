import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-qcm-history',
  templateUrl: './qcm-history.component.html',
  styleUrls: ['./qcm-history.component.scss']
})
export class QcmHistoryComponent {
  @Input() qcm: any;
}
