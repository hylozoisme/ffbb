import { Component, Input } from '@angular/core';
import {QcmService} from "../../../../core/services/qcm.service";

@Component({
  selector: 'app-feedback',
  templateUrl: './feedback.component.html',
  styleUrls: ['./feedback.component.scss']
})
export class FeedbackComponent {
  @Input() feedback: any;
  isUserCorrect: any;

  constructor(public qcmService: QcmService) { }


  ngOnChanges(): void {
    this.isUserCorrect = this.qcmService.isUserAnswerCorrect();
  }

}
