import { Component, Input, Output, EventEmitter } from '@angular/core';
import { faArrowRight, faCheck } from '@fortawesome/free-solid-svg-icons';
import { QcmService } from "../../../../core/services/qcm.service";

@Component({
  selector: 'app-answer',
  templateUrl: './answer.component.html',
  styleUrls: ['./answer.component.scss']
})
export class AnswerComponent {
  @Input() choices: any;
  @Output() answers = new EventEmitter<any>();

  selectedChoices: Set<any> = new Set();
  isValidate = false;
  isLastQuestion = false;

  faArrowRight = faArrowRight;
  faCheck = faCheck;

  isCorrectAnswer: boolean | null = null;
  userAnswerStatusSubscription: any

  constructor(public qcmService: QcmService) {
    this.userAnswerStatusSubscription = this.qcmService._userAnswerStatus.subscribe(isCorrect => {
      this.isCorrectAnswer = isCorrect;
    });

    this.isLastQuestion = this.qcmService.isLastQuestion();
  }

  checkAnswer(choiceInput: HTMLInputElement, wrapper: HTMLElement) {
    choiceInput.checked = !choiceInput.checked;
    wrapper.classList.toggle('checked');

    if (choiceInput.checked) {
      this.selectedChoices.add(choiceInput.id);
    } else {
      this.selectedChoices.delete(choiceInput.id);
    }
  }

  submitAnswer() {
    if (this.selectedChoices.size === 0) {
      return;
    }
    this.isValidate = true;
    this.answers.emit(Array.from(this.selectedChoices));
  }

  nextQuestion() {
    this.qcmService.goToNextQuestion();
    this.selectedChoices.clear();
    this.isValidate = false;
    this.isLastQuestion = this.qcmService.isLastQuestion();
  }

  isChoiceCorrect(choiceValue: any): boolean {
    if (!this.qcmService.getAnswerResponse()) {
      return false;
    }

    const correctAnswerIds = this.qcmService.getCorrectAnswerIds();
    return correctAnswerIds.includes(choiceValue);
  }

  viewResults() {
    this.qcmService.viewResults();
  }
}
