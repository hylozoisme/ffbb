import { Component, NgZone } from '@angular/core';
import { NavigationStart, Router } from '@angular/router';
import { NotificationService } from './core/services/notification.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  show = true;
  title = 'refpath';

  constructor(
    private router: Router,
    private notification: NotificationService,
    private ngZone: NgZone
  ) {
    router.events.forEach((event) => {
      if (event instanceof NavigationStart) {
        this.ngZone.runOutsideAngular(() => {
          setTimeout(() => {
            this.show = event['url'] != '/connexion' && event['url'] != '/inscription';
          });
        });
      }
    });
  }
}
