import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {HomeComponent} from "./modules/home/home.component";
import {LoginComponent} from "./modules/login/login.component";
import {RegisterComponent} from "./modules/register/register.component";
import {AuthGuard} from "./core/authentification/auth.guard";
import {LesReglesComponent} from "./modules/les-regles/les-regles.component";
import {LesIntepretationsComponent} from "./modules/les-intepretations/les-intepretations.component";
import {ArticleComponent} from "./modules/article/article.component";
import {EntrainementComponent} from "./modules/entrainement/entrainement.component";
import {QcmComponent} from "./modules/qcm/qcm.component";
import {QcmResultComponent} from "./modules/qcm-result/qcm-result.component";
import {ProfilComponent} from "./modules/profil/profil.component";

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'entrainement',
    component: EntrainementComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'entrainement/:uuid',
    component: QcmComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'entrainement/:uuid/result',
    component: QcmResultComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'reglement/toutes-les-regles',
    component: LesReglesComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'reglement/toutes-les-intepretations',
    component: LesIntepretationsComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'reglement/:article_parent_slug/:id',
    component: ArticleComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'profil',
    component: ProfilComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'connexion',
    component: LoginComponent,
  },
  {
    path: 'inscription',
    component: RegisterComponent
  },
  {
    //404
    path: '**',
    redirectTo: ''
  },
  {
    //500
    path: 'error',
    redirectTo: ''
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { scrollPositionRestoration: 'enabled' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
