import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LesIntepretationsComponent } from './les-intepretations.component';

describe('LesIntepretationsComponent', () => {
  let component: LesIntepretationsComponent;
  let fixture: ComponentFixture<LesIntepretationsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LesIntepretationsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(LesIntepretationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
