import { Component, OnInit } from '@angular/core';
import {QcmService} from "../../core/services/qcm.service";
import {ActivatedRoute, Router} from "@angular/router";
import {Subscription} from "rxjs";

@Component({
  selector: 'app-qcm',
  templateUrl: './qcm.component.html',
  styleUrls: ['./qcm.component.scss']
})
export class QcmComponent {
  uuid = '';
  currentQuestionIndex = 0;
  question: any;

  constructor(public qcmService: QcmService, private activatedRoute: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.getQcm();
  }


  getQcm() {
    this.activatedRoute.params.subscribe(params => {
      this.uuid = params['uuid'];
      this.qcmService.getQcm(this.uuid);
      this.qcmService.getCurrentQuestionIndex().subscribe(
        (index: number) => {
          this.currentQuestionIndex = index;
        })
    });
  }
}
