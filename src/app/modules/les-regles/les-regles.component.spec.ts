import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LesReglesComponent } from './les-regles.component';

describe('LesReglesComponent', () => {
  let component: LesReglesComponent;
  let fixture: ComponentFixture<LesReglesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LesReglesComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(LesReglesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
