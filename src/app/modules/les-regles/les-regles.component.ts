import { Component } from '@angular/core';
import {ReglesService} from "../../core/services/regles.service";

interface Article {
  children: Article[];
  id: number;
  libelle: string;
  parent?: {
    libelle: string;
    user_learned: any[];
  };
  user_learned: any[];
}

interface Rule {
  id: number;
  libelle: string;
  articles: Article[];
}


@Component({
  selector: 'app-les-regles',
  templateUrl: './les-regles.component.html',
  styleUrls: ['./les-regles.component.scss']
})
export class LesReglesComponent {
  regles: any;
  loading = true;
  nbOfSquare = [0, 1, 2, 3, 4, 5];

  constructor(private reglesService: ReglesService) {
    this.getRegles();
  }

  getRegles() {
    this.reglesService.getRegles().subscribe((data) => {
      this.regles = data;
    }, (error) => {
        console.log(error);
    }, () => {
      this.loading = false;
    });
  }
}
