import {Component, OnInit, Renderer2} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import { faClock } from '@fortawesome/free-regular-svg-icons';
import {ArticleService} from "../../core/services/article.service";

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.scss'],
})
export class ArticleComponent {

  articleParentSlug!: string;
  id!: string;
  article: any;
  articleParent: any;
  faClock = faClock;

  constructor(private route: ActivatedRoute, private articleService: ArticleService){
  }

  ngOnInit() {
    this.article = null;
    this.route.params.subscribe(params => {
      this.articleParentSlug = params['article_parent_slug'];
      this.id = params['id'];

      this.articleService.getArticle(this.id).subscribe((article: any) => {
        this.article = article;
      })

      this.articleService.getArticle(this.articleParentSlug).subscribe((article: any) => {
        this.articleParent = article;
      })
    });
  }
}
