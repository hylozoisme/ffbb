import { Component } from '@angular/core';
import {QcmService} from "../../core/services/qcm.service";
import {ActivatedRoute, Router} from "@angular/router";
import { faChevronRight } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-qcm-result',
  templateUrl: './qcm-result.component.html',
  styleUrls: ['./qcm-result.component.scss']
})
export class QcmResultComponent {
    qcm: any;
    faChevronRight = faChevronRight;
    loading = true;

    constructor(private qcmService: QcmService,
                private activatedRoute: ActivatedRoute,
                private router: Router) {
      this.activatedRoute.params.subscribe(params => {
        this.qcmService.getQcmByUuid(params['uuid']).subscribe(qcm => {
          this.qcm = qcm;
          this.loading = false;
        })
      })
    }

}
