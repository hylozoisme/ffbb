import { Component, OnInit, OnChanges } from '@angular/core';
import {QcmService} from "../../core/services/qcm.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-profil',
  templateUrl: './profil.component.html',
  styleUrls: ['./profil.component.scss']
})
export class ProfilComponent {
  qcms: any[] = [];
  form!: FormGroup;
  error: any = null;
  loading = false;

  constructor(public qcmService: QcmService, private formBuilder: FormBuilder,) { }

  ngOnInit(): void {
    this.form = this.formBuilder.group({
      nom: [''],
      prenom: [''],
      username: [''],
    });

    this.qcmService.getAllQcms().subscribe((data: any) => {
      this.qcms = data;
    })
  }

  save() {
    console.log(this.form.value);
  }
}
