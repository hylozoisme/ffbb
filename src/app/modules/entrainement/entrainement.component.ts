import {Component, Renderer2} from '@angular/core';
import {faArrowRight, faBolt, faVideo, faStar, faChartLine } from '@fortawesome/free-solid-svg-icons';
import {QcmService} from "../../core/services/qcm.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-entrainement',
  templateUrl: './entrainement.component.html',
  styleUrls: ['./entrainement.component.scss']
})
export class EntrainementComponent {
  faStar = faStar;
  faBolt = faBolt;
  faArrowRight = faArrowRight;
  faVideo = faVideo;
  faChartLine = faChartLine;
  uuid: any;
  qcms: any[] = [];
  currentPage = 1;
  avg = 0;

  constructor(private qcmService: QcmService, private router: Router, private renderer2: Renderer2) { }

  ngOnInit(): void {
    this.getQcms(this.currentPage);
  }

  generateQcm() {
    this.qcmService.generateQcm().subscribe(
      (data) => {
        this.uuid = data;
        this.router.navigate(['/entrainement/' + this.uuid.id]);
      }
    );
  }

  getQcms(page: number){
    this.qcmService.getQcms(page).subscribe(
      (data: any) => {
        if (data.length > 0) {
          this.qcms = this.qcms.concat(data);
          this.avg = Math.round(data.reduce((acc: number, qcm: any) => acc + qcm.note, 0) / data.length);
        } else {
          this.renderer2.addClass(document.querySelector('.btn-voir-plus'), 'disabled');
        }
      }, (error) => {
        console.log(error);
      }
    );
  }




  nextPage() {
    this.getQcms(this.currentPage + 1);
    this.currentPage++;
  }
}
