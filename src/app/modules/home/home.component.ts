import {Component, ViewChild, Renderer2} from '@angular/core';
import {QuestionService} from "../../core/services/question.service";
import {Question} from "../../shared/models/question";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent {
  questions: Question[] = [];

  constructor(private questionService: QuestionService, private renderer: Renderer2) {
  }

  makeDivShake() {
    const quizzContainer = document.querySelector('.quizz-container');
    //make the div shake
    this.renderer.addClass(quizzContainer, 'shake');
    //remove the shake class after 1 second
    setTimeout(() => {
      this.renderer.removeClass(quizzContainer, 'shake');
    }, 350);
  }
}
