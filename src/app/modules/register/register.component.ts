import {Component, OnInit, ViewChild} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {Router} from "@angular/router";
import { faExclamationCircle } from '@fortawesome/free-solid-svg-icons';
import {AuthService} from "../../core/authentification/auth.service";
import {NotificationService} from "../../core/services/notification.service";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  @ViewChild('loginButton') loginButton!: any;

  form!: FormGroup;
  error: any = null;
  loading = false;
  faExclamationCircle = faExclamationCircle;

  constructor(private formBuilder: FormBuilder,
              private auth: AuthService,
              private router: Router,
              private notification: NotificationService) { }

  ngOnInit() {
    this.form = this.formBuilder.group({
      nom: ['', [Validators.required]],
      prenom: ['', [Validators.required]],
      username: ['', [Validators.required]],
      password: ['', [Validators.required], [Validators.pattern('(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{8,}')]],
      confirmPassword: ['', [Validators.required]]
    });
  }

  register() {
    this.loginButton.nativeElement.disabled = true;
    this.loading = true;

    if (this.form.invalid || this.form.get('password')?.value !== this.form.get('confirmPassword')?.value) {
      this.loginButton.nativeElement.disabled = false;
      this.loading = false;

      return;
    } else {
      this.auth.register(
        this.form.get('username')?.value,
        this.form.get('password')?.value,
        this.form.get('nom')?.value,
        this.form.get('prenom')?.value)

        .subscribe((response) => {
          this.notification.sendNotification('Votre compte a été créé avec succès !');
          this.router.navigate(['/']);
        }, (error) => {
          this.loginButton.nativeElement.disabled = false;
          if(error?.error?.message) {
            this.error = error?.error?.message;
          } else {
            this.error = 'Une erreur est survenue lors de la création de votre compte.';
          }
        });

      this.loading = false;

      return;
    }
  }
}
