import { LOCALE_ID, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { HomeComponent } from './modules/home/home.component';
import { HeaderComponent } from './core/header/header.component';
import { FooterComponent } from './core/footer/footer.component';
import { LoginComponent } from './modules/login/login.component';
import { RegisterComponent } from './modules/register/register.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatProgressSpinnerModule } from "@angular/material/progress-spinner";
import { MatInputModule } from "@angular/material/input";
import { MatCheckboxModule } from "@angular/material/checkbox";
import { AuthInterceptor } from "./core/interceptor/authconfig.interceptor";
import { LesReglesComponent } from './modules/les-regles/les-regles.component';
import { LesIntepretationsComponent } from './modules/les-intepretations/les-intepretations.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ReglesComponent } from './shared/components/regles/regles.component';
import { ReglesArticleComponent } from './shared/components/regles/regles-article/regles-article.component';
import { ModalArticleComponent } from './shared/components/modal-article/modal-article.component';
import { ArticleComponent } from './modules/article/article.component';
import { Safe } from './core/safe-html.pipe';
import { ArticleNavigationComponent } from './shared/components/article-navigation/article-navigation.component';
import { EntrainementComponent } from './modules/entrainement/entrainement.component';
import { QcmComponent } from './modules/qcm/qcm.component';
import { QuestionComponent } from './shared/components/qcm/question/question.component';
import { AnswerComponent } from './shared/components/qcm/answer/answer.component';
import { FeedbackComponent } from './shared/components/qcm/feedback/feedback.component';
import { QcmHistoryComponent } from './shared/components/qcm/qcm-history/qcm-history.component';
import { SafeUrlPipe } from './core/safeurl.pipe';
import { QcmResultComponent } from './modules/qcm-result/qcm-result.component';
import { GoBackComponent } from './shared/components/go-back/go-back.component';
import { QuestionCorrectionComponent } from './shared/components/qcm/question-correction/question-correction.component';
import { NotificationComponent } from './shared/components/notification/notification.component';
import { ErrorComponent } from './shared/components/error/error.component';
import { NgChartsModule } from 'ng2-charts';
import { ChartComponent } from './shared/components/chart/chart.component';
import { ProfilComponent } from './modules/profil/profil.component';

registerLocaleData(localeFr);

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent,
    FooterComponent,
    LoginComponent,
    RegisterComponent,
    LesReglesComponent,
    LesIntepretationsComponent,
    ReglesComponent,
    ReglesArticleComponent,
    ModalArticleComponent,
    ArticleComponent,
    Safe,
    ArticleNavigationComponent,
    EntrainementComponent,
    QcmComponent,
    QuestionComponent,
    AnswerComponent,
    FeedbackComponent,
    QcmHistoryComponent,
    SafeUrlPipe,
    QcmResultComponent,
    GoBackComponent,
    QuestionCorrectionComponent,
    NotificationComponent,
    ErrorComponent,
    ChartComponent,
    ProfilComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatProgressSpinnerModule,
    MatInputModule,
    HttpClientModule,
    MatCheckboxModule,
    FontAwesomeModule,
    FormsModule,
    NgChartsModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true,
    },
    { provide: LOCALE_ID, useValue: "fr-FR" },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
